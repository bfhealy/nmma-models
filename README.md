# NMMA models

This is a gitlab repo used to host the NMMA models. A script is also provided to prepare the models (by compressing them) and to generate the models.yaml file used by the NMMA python package when fetching models.